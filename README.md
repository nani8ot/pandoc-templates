# Pandoc Templates

My pandoc templates so I can start writing immediatly.

# Credits
The "pic.png" is found in the repo of [Neovim](https://github.com/neovim/neovim) and is licensed under [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
