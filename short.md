---
title: "Titel"
subtitle:
author: Aaron Wiedemer
date: 2020
---
---
lang: de-DE
toc: false
suppress-bibliography: false
link-citations: true
papersize: A4
geometry: margin=3cm
linestretch: 1
csquotes: true
mainfont: "Constantia"
sansfont: "Calibri"
fontsize: 11pt

header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead[R]{\slshape \leftmark}
    \fancyhead[L]{\slshape \rightmark}
    \renewcommand{\familydefault}{\rmdefault}
    \usepackage{blindtext}
    \usepackage{sectsty}
    \usepackage[german=quotes]{csquotes}
    \allsectionsfont{\sffamily}
---
# Überschrift 1

\Blindtext
